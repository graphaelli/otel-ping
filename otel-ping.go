package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	v1 "github.com/census-instrumentation/opencensus-proto/gen-go/agent/trace/v1"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/plog"
	"go.opentelemetry.io/collector/pdata/plog/plogotlp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetricgrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/metric/global"
	"go.opentelemetry.io/otel/metric/instrument"
	"go.opentelemetry.io/otel/propagation"
	metriccontroller "go.opentelemetry.io/otel/sdk/metric/controller/basic"
	metricprocessor "go.opentelemetry.io/otel/sdk/metric/processor/basic"
	"go.opentelemetry.io/otel/sdk/metric/selector/simple"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	grpcinsecure "google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func grpcExporters(ctx context.Context, endpoint, token string, insecure, verify bool) (*otlptrace.Exporter, *otlpmetric.Exporter, plogotlp.Client, error) {
	traceOpt := []otlptracegrpc.Option{
		otlptracegrpc.WithEndpoint(endpoint),
	}
	metricOpt := []otlpmetricgrpc.Option{
		otlpmetricgrpc.WithEndpoint(endpoint),
	}
	var logOpt []grpc.DialOption
	dialOptions := []grpc.DialOption{
		grpc.WithBlock(),
		grpc.WithUnaryInterceptor(func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
			//opts = append(opts, grpc.ForceCodec(NewDebugCodec(512)))
			err := invoker(ctx, method, req, reply, cc, opts...)
			if err != nil {
				if rep, ok := reply.(*v1.ExportTraceServiceResponse); ok {
					log.Println(rep.String())
				}
				stat := status.Convert(err)
				log.Println("details:", stat.Details(), "msg:", stat.Message())
			}
			return err
		}),
	}
	if insecure {
		traceOpt = append(traceOpt, otlptracegrpc.WithInsecure())
		metricOpt = append(metricOpt, otlpmetricgrpc.WithInsecure())
		logOpt = append(logOpt, grpc.WithTransportCredentials(grpcinsecure.NewCredentials()))
	} else {
		var config *tls.Config
		if !verify {
			config = &tls.Config{
				InsecureSkipVerify: true,
			}
		}
		dialOptions = append(dialOptions, grpc.WithTransportCredentials(credentials.NewTLS(config)))
	}
	if token != "" {
		traceOpt = append(traceOpt, otlptracegrpc.WithHeaders(map[string]string{"Authorization": "Bearer " + token}))
		metricOpt = append(metricOpt, otlpmetricgrpc.WithHeaders(map[string]string{"Authorization": "Bearer " + token}))
	}
	traceOpt = append(traceOpt, otlptracegrpc.WithDialOption(dialOptions...))
	metricOpt = append(metricOpt, otlpmetricgrpc.WithDialOption(dialOptions...))
	for _, opt := range dialOptions {
		logOpt = append(logOpt, opt)
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	traceExporter, err := otlptracegrpc.New(ctx, traceOpt...)
	if err != nil {
		return nil, nil, nil, err
	}
	metricExporter, err := otlpmetricgrpc.New(ctx, metricOpt...)
	if err != nil {
		return nil, nil, nil, err
	}

	if token != "" {
		md := metadata.New(map[string]string{"Authorization": "Bearer " + token})
		ctx = metadata.NewOutgoingContext(ctx, md)
	}
	logConn, err := grpc.DialContext(ctx, endpoint, logOpt...)
	if err != nil {
		return nil, nil, nil, err
	}
	logClient := plogotlp.NewClient(logConn)
	return traceExporter, metricExporter, logClient, nil
}

func httpExporter(ctx context.Context, endpoint, token string, insecure, verify bool) (*otlptrace.Exporter, *otlpmetric.Exporter, error) {
	traceOpt := []otlptracehttp.Option{
		otlptracehttp.WithEndpoint(endpoint),
	}
	metricOpt := []otlpmetrichttp.Option{
		otlpmetrichttp.WithEndpoint(endpoint),
	}
	if insecure {
		traceOpt = append(traceOpt, otlptracehttp.WithInsecure())
		metricOpt = append(metricOpt, otlpmetrichttp.WithInsecure())
	} else {
		var config *tls.Config
		if !verify {
			config = &tls.Config{
				InsecureSkipVerify: true,
			}
		}
		traceOpt = append(traceOpt, otlptracehttp.WithTLSClientConfig(config))
		metricOpt = append(metricOpt, otlpmetrichttp.WithTLSClientConfig(config))
	}
	if token != "" {
		traceOpt = append(traceOpt, otlptracehttp.WithHeaders(map[string]string{"Authorization": "Bearer " + token}))
		metricOpt = append(metricOpt, otlpmetrichttp.WithHeaders(map[string]string{"Authorization": "Bearer " + token}))
	}

	traceExporter, err := otlptracehttp.New(ctx, traceOpt...)
	if err != nil {
		return nil, nil, err
	}
	metricExporter, err := otlpmetrichttp.New(ctx, metricOpt...)
	if err != nil {
		return nil, nil, err
	}
	return traceExporter, metricExporter, nil

}

func getEnv(key string, fallback string) string {
	v := os.Getenv(key)
	if v == "" {
		return fallback
	}
	return v
}

func main() {
	count := flag.Int("c", 0, "ping count")
	endpoint := flag.String("e", "localhost:8200", "OTLP endpoint")
	token := flag.String("t", "", "APM secret token")
	insecure := flag.Bool("k", false, "insecure")
	verify := flag.Bool("verify", false, "don't skip TLS verification")
	useHTTP := flag.Bool("http", false, "use HTTP instead of the default GRPC")
	flag.Parse()
	ctx := context.Background()

	var traceExporter *otlptrace.Exporter
	var metricExporter *otlpmetric.Exporter
	var logsClient plogotlp.Client
	var err error
	if *useHTTP {
		traceExporter, metricExporter, err = httpExporter(ctx, *endpoint, *token, *insecure, *verify)
	} else {
		traceExporter, metricExporter, logsClient, err = grpcExporters(ctx, *endpoint, *token, *insecure, *verify)
	}
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err := traceExporter.Shutdown(ctx)
		if err != nil {
			log.Printf("failed to stop trace exporter: %v", err)
		}
	}()
	defer func() {
		err := metricExporter.Shutdown(ctx)
		if err != nil {
			log.Printf("failed to stop metric exporter: %v", err)
		}
	}()

	res, err := resource.New(ctx,
		resource.WithAttributes(
			// the service name used to display traces in backends
			semconv.ServiceNameKey.String(getEnv("OTEL_SERVICE_NAME", "otel-ping")),
			semconv.ServiceNamespaceKey.String(getEnv("X_OTEL_SERVICE_NAMESPACE", "staging-ns")),
			semconv.ServiceVersionKey.String(getEnv("X_OTEL_SERVICE_VERISON", "0.1.0")),
			semconv.DeploymentEnvironmentKey.String(getEnv("X_OTEL_DEPLOYMENT_ENVIRONMENT", "staging-env")),
		),
	)
	if err != nil {
		log.Fatalf("failed to create resource: %v", err)
	}

	ssp := sdktrace.NewSimpleSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(ssp),
	)
	otel.SetTextMapPropagator(propagation.TraceContext{})
	otel.SetTracerProvider(tracerProvider)
	tracer := otel.Tracer("ping")

	metricPusher := metriccontroller.New(
		metricprocessor.NewFactory(
			simple.NewWithHistogramDistribution(),
			metricExporter,
		),
		metriccontroller.WithResource(res),
		metriccontroller.WithExporter(metricExporter),
		metriccontroller.WithCollectPeriod(2*time.Second),
	)
	global.SetMeterProvider(metricPusher)

	if err := metricPusher.Start(ctx); err != nil {
		log.Fatalf("could not start metric controller: %v", err)
	}
	defer func() {
		ctx, cancel := context.WithTimeout(ctx, time.Second)
		defer cancel()
		// pushes any last exports to the receiver
		if err := metricPusher.Stop(ctx); err != nil {
			otel.Handle(err)
		}
	}()

	meter := metricPusher.Meter("otel-ping")
	pc, err := meter.SyncInt64().Counter(
		"ping.count",
		instrument.WithUnit("pings"),
		instrument.WithDescription("Just a test counter"),
	)
	if err != nil {
		log.Fatal(err)
	}

	if logsClient != nil {
		// just so we can set the trace/span ids on the log
		_, span := tracer.Start(ctx, "ping-log")
		logs := plog.NewLogs()

		rl := logs.ResourceLogs().AppendEmpty()
		attribs := rl.Resource().Attributes()
		for _, attr := range res.Attributes() {
			switch attr.Value.Type() {
			case attribute.INT64:
				attribs.Insert(string(attr.Key), pcommon.NewValueInt(attr.Value.AsInt64()))
			case attribute.STRING:
				attribs.Insert(string(attr.Key), pcommon.NewValueString(attr.Value.AsString()))
			}
		}
		sl := rl.ScopeLogs().AppendEmpty().LogRecords()
		record := sl.AppendEmpty()
		record.Body().SetStringVal("test record")
		record.SetTimestamp(pcommon.NewTimestampFromTime(time.Now()))
		record.SetTraceID(pcommon.NewTraceID(span.SpanContext().TraceID()))
		record.SetSpanID(pcommon.NewSpanID(span.SpanContext().SpanID()))
		record.SetSeverityNumber(plog.SeverityNumberFATAL)
		record.SetSeverityText("fatal")

		req := plogotlp.NewRequestFromLogs(logs)
		log.Println("sending log")
		md := metadata.New(map[string]string{"Authorization": "Bearer " + *token})
		ctx = metadata.NewOutgoingContext(ctx, md)

		rsp, err := logsClient.Export(ctx, req)
		if err != nil {
			log.Print(err)
		}
		b, _ := rsp.MarshalJSON()
		log.Println(string(b))
		log.Printf("sent log with trace.id:%s span.id:%s", span.SpanContext().TraceID().String(), span.SpanContext().SpanID())
		span.End()
	}

	for c := 0; c < *count || *count == 0; c++ {
		pc.Add(ctx, 1)
		spanCtx, span := tracer.Start(ctx, "ping")
		span.AddEvent("ping start")
		_, cspan := tracer.Start(spanCtx, "ping progress...")
		for i := 1; i < 7; i++ {
			span.AddEvent(fmt.Sprintf("operation progressing %d/10", i))
			time.Sleep(100 * time.Millisecond)
		}
		cspan.End()
		span.SetStatus(codes.Ok, "fail")
		span.AddEvent("ping complete")
		span.End()
		log.Print(span.SpanContext().TraceID().String())
	}
}
